package br.com.gpm.concretegithub.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import br.com.gpm.concretegithub.R;

public class ProgressViewHolder extends RecyclerView.ViewHolder {
    ProgressBar progressBar;

    public ProgressViewHolder(View itemView) {
        super(itemView);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
    }
}
