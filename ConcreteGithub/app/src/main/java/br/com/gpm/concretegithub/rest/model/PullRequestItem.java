package br.com.gpm.concretegithub.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by usuario on 01/08/16.
 */
public class PullRequestItem {
    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("url")
    private String url;

    @SerializedName("user")
    private User user;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
