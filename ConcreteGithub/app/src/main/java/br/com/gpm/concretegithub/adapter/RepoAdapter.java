package br.com.gpm.concretegithub.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import br.com.gpm.concretegithub.R;
import br.com.gpm.concretegithub.rest.model.GithubRepository;

/**
 * Created by usuario on 26/07/16.
 */
public class RepoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static int VIEW_ITEM = 1;
    private static int VIEW_PROGRESS = 0;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private List<GithubRepository> list = Collections.emptyList();
    private Context context;

    public RepoAdapter(List<GithubRepository> list, RecyclerView recyclerView, Context context) {
        this.list = list;
        this.context = context;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if(viewType == VIEW_ITEM){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout_repositories, viewGroup, false);
            viewHolder = new RepoViewHolder(view);
        }
        else{
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.progressbar_item, viewGroup, false);
            viewHolder = new ProgressViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof RepoViewHolder){
            ((RepoViewHolder) holder).repoName.setText(list.get(i).getName());
            ((RepoViewHolder) holder).repoDescription.setText(list.get(i).getDescription());
            ((RepoViewHolder) holder).forkCount.setText(Integer.toString(list.get(i).getForksCount()));
            ((RepoViewHolder) holder).starsCount.setText(Integer.toString(list.get(i).getStargazersCount()));
            ((RepoViewHolder) holder).username.setText(list.get(i).getOwner().getLogin());

            Picasso.with(context)
                    .load(list.get(i).getOwner().getAvatarUrl())
                    .placeholder(R.drawable.github_placeholder)
                    .into(((RepoViewHolder) holder).userAvatar);
        }
        else{
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROGRESS;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }
}
