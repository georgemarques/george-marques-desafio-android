package br.com.gpm.concretegithub.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.gpm.concretegithub.R;

/**
 * Created by usuario on 26/07/16.
 */
public class RepoViewHolder extends RecyclerView.ViewHolder {
    CardView cv;
    TextView repoName;
    TextView repoDescription;
    TextView forkCount;
    TextView starsCount;
    TextView username;
    ImageView userAvatar;

    public RepoViewHolder(View itemView) {
        super(itemView);

        cv = (CardView) itemView.findViewById(R.id.cardView);
        repoName = (TextView) itemView.findViewById(R.id.repoNameTv);
        repoDescription = (TextView) itemView.findViewById(R.id.repoDescriptionTv);
        forkCount = (TextView) itemView.findViewById(R.id.forksCountTv);
        starsCount = (TextView) itemView.findViewById(R.id.starsCountTv);
        username = (TextView) itemView.findViewById(R.id.userId);
        userAvatar = (ImageView) itemView.findViewById(R.id.userAvatar);
    }
}
