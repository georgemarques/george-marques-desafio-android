package br.com.gpm.concretegithub.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.gpm.concretegithub.R;

/**
 * Created by usuario on 01/08/16.
 */
public class PullRequestViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView pullTitle;
        TextView pullBody;
        TextView pullUserLogin;        
        ImageView pullUserAvatar;

    public PullRequestViewHolder(View itemView) {
        super(itemView);

        cv = (CardView) itemView.findViewById(R.id.cardViewPulls);
        pullTitle = (TextView) itemView.findViewById(R.id.pullTitle);
        pullBody = (TextView) itemView.findViewById(R.id.pullBody);
        pullUserLogin = (TextView) itemView.findViewById(R.id.pullUserLogin);
        pullUserAvatar = (ImageView) itemView.findViewById(R.id.pullUserAvatar);
    }
}