package br.com.gpm.concretegithub.rest.model;

import java.util.List;

/**
 * Created by usuario on 24/07/16.
 */
public class RepositoryResponse {
    private List<GithubRepository> items;

    public List<GithubRepository> getItems() {
        return items;
    }
}
