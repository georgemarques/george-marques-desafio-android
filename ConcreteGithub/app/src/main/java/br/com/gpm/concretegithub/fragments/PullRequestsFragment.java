package br.com.gpm.concretegithub.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.gpm.concretegithub.Globals;
import br.com.gpm.concretegithub.R;
import br.com.gpm.concretegithub.adapter.PullRequestAdapter;
import br.com.gpm.concretegithub.adapter.RepoAdapter;
import br.com.gpm.concretegithub.rest.GithubApi;
import br.com.gpm.concretegithub.rest.model.GithubRepository;
import br.com.gpm.concretegithub.rest.model.PullRequestItem;
import br.com.gpm.concretegithub.rest.model.PullResponse;
import br.com.gpm.concretegithub.rest.model.RepositoryResponse;
import br.com.gpm.concretegithub.utils.Constants;
import br.com.gpm.concretegithub.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by usuario on 01/08/16.
 */
public class PullRequestsFragment extends Fragment{
    public static final String TAG = "PullRequestsFragmentTAG";

    @BindView(R.id.recyclerViewPullRequest)
    RecyclerView recyclerView;

    @BindView(R.id.progressBarPullRequest)
    ProgressBar progressBar;

    private Retrofit retrofit;
    private GithubApi githubApi;
    private List<PullRequestItem> pullRequestList;
    private PullRequestAdapter adapter;
    private String requestName, requestLogin;

    //Constructor
    public PullRequestsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_pull_requests, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if (bundle != null){
            requestLogin = bundle.getString(Constants.PULL_REPO_LOGIN_EXTRA);
            requestName = bundle.getString(Constants.PULL_REPO_NAME_EXTRA);
        }

        recyclerView.setHasFixedSize(true);
        retrofit = Utils.buildRetrofit();
        githubApi = retrofit.create(GithubApi.class);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        pullRequestList = new ArrayList<PullRequestItem>();
        adapter = new PullRequestAdapter(pullRequestList, view.getContext());
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        fetchPullRequestsData();
    }

    public void fetchPullRequestsData(){
        Call<List<PullRequestItem>> callPulls = githubApi.getPullRequests(requestLogin, requestName);
        callPulls.enqueue(new Callback<List<PullRequestItem>>() {
            @Override
            public void onResponse(Call<List<PullRequestItem>> call, Response<List<PullRequestItem>> response) {
                progressBar.setVisibility(View.GONE);
                pullRequestList.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<PullRequestItem>> call, Throwable t) {
                Toast.makeText(getActivity(), "Retrofit Error", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

}


