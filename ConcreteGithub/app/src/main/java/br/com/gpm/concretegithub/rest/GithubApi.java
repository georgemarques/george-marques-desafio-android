package br.com.gpm.concretegithub.rest;

import java.util.List;

import br.com.gpm.concretegithub.rest.model.PullRequestItem;
import br.com.gpm.concretegithub.rest.model.PullResponse;
import br.com.gpm.concretegithub.rest.model.RepositoryResponse;
import br.com.gpm.concretegithub.rest.model.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by usuario on 24/07/16.
 */
public interface GithubApi {
    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoryResponse> getRepositories(@Query("page") int pageNumber);

    @GET("users/{login}")
    Call<User> getUser(@Path("login") String login);

    @GET("repos/{login}/{name}/pulls")
    Call<List<PullRequestItem>> getPullRequests(@Path("login") String login, @Path("name") String name);

}
