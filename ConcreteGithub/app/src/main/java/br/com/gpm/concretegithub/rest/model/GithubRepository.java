package br.com.gpm.concretegithub.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by usuario on 24/07/16.
 */
public class GithubRepository{
    private int id;
    private String name;
    private String description;
    private Owner owner;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("stargazers_count")
    private int stargazersCount;

    public Owner getOwner() {
        return owner;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getFullName() {
        return fullName;
    }

    public int getForksCount() {
        return forksCount;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }
}
