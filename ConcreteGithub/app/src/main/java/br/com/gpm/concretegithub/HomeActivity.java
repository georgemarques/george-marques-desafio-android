package br.com.gpm.concretegithub;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import br.com.gpm.concretegithub.fragments.RepositoriesFragment;

public class HomeActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        startRepositoriesFragment();
    }

    private void startRepositoriesFragment(){
        getSupportFragmentManager().beginTransaction()
                .add(R.id.reposFrameLayout, new RepositoriesFragment())
                .commit();
    }
}
