package br.com.gpm.concretegithub.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by usuario on 24/07/16.
 */
public class Owner {
    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
