package br.com.gpm.concretegithub.utils;

/**
 * Created by usuario on 24/07/16.
 */
public final class Constants {
    public static final String BASE_URL = "https://api.github.com/";
    public static final String PULL_REPO_NAME_EXTRA = "pull_request_name";
    public static final String PULL_REPO_LOGIN_EXTRA = "pull_request_login";
}
