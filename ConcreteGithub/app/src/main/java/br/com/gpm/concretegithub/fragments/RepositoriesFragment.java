package br.com.gpm.concretegithub.fragments;


import br.com.gpm.concretegithub.Globals;
import br.com.gpm.concretegithub.R;
import br.com.gpm.concretegithub.adapter.OnLoadMoreListener;
import br.com.gpm.concretegithub.adapter.RecyclerItemClickListener;
import br.com.gpm.concretegithub.adapter.RepoAdapter;
import br.com.gpm.concretegithub.rest.GithubApi;
import br.com.gpm.concretegithub.rest.model.GithubRepository;
import br.com.gpm.concretegithub.rest.model.RepositoryResponse;
import br.com.gpm.concretegithub.utils.Constants;
import br.com.gpm.concretegithub.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class RepositoriesFragment extends Fragment {
    public static final String TAG = "RepositoriesFragmentTAG";
    public static RepositoriesFragment newInstance(){
        return new RepositoriesFragment();
    }
    private Retrofit retrofit;
    private GithubApi githubApi;
    private List<GithubRepository> reposList;
    private RepoAdapter adapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressBarFragment)
    ProgressBar progressBarFragment;

    //Constructor
    public RepositoriesFragment(){ }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_repositories_list, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setHasFixedSize(true);

        retrofit = Utils.buildRetrofit();
        githubApi = retrofit.create(GithubApi.class);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        reposList = new ArrayList<GithubRepository>();
        adapter = new RepoAdapter(reposList, recyclerView, view.getContext());
        adapter.setOnLoadMoreListener(loadMoreListener);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(itemClickListener);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(reposList == null || reposList.size() == 0){
            Globals.currentPage = 1;
            fetchRepoData();
        }
    }

    public void fetchRepoData(){
        Call<RepositoryResponse> callRepositories = githubApi.getRepositories(Globals.currentPage);
        callRepositories.enqueue(new Callback<RepositoryResponse>() {
            @Override
            public void onResponse(Call<RepositoryResponse> call, Response<RepositoryResponse> response) {
                RepositoryResponse repositoryResponse = response.body();
                int lastPosition = 0;
                if(reposList != null && reposList.size()> 0) {
                    //remove progress item
                    reposList.remove(reposList.size() - 1);
                    adapter.notifyItemRemoved(reposList.size());
                }

                if(reposList == null){
                    reposList = repositoryResponse.getItems();
                }
                else{
                    lastPosition = reposList.size();
                    reposList.addAll(repositoryResponse.getItems());
                }

                adapter.notifyItemInserted(lastPosition);
                adapter.setLoaded();
                Globals.currentPage++;
                progressBarFragment.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<RepositoryResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Retrofit Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    // Listeners implementation
    OnLoadMoreListener loadMoreListener = new OnLoadMoreListener() {
        @Override
        public void onLoadMore() {
            //add progress item
            reposList.add(null);
            adapter.notifyItemInserted(reposList.size() - 1);

            fetchRepoData();
        }
    };

    RecyclerItemClickListener itemClickListener =
            new RecyclerItemClickListener(getContext(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                @Override public void onItemClick(View view, int position) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.PULL_REPO_LOGIN_EXTRA, reposList.get(position).getOwner().getLogin());
                    bundle.putString(Constants.PULL_REPO_NAME_EXTRA, reposList.get(position).getName());
                    startPullsFragment(bundle);
                }

                @Override public void onLongItemClick(View view, int position) {
                    // do whatever
                }
     });

    private void startPullsFragment(Bundle bundle){
        PullRequestsFragment fragment = new PullRequestsFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.reposFrameLayout, fragment)
                .addToBackStack(TAG)
                .commit();
    }

}

