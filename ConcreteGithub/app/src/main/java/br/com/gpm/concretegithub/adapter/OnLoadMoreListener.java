package br.com.gpm.concretegithub.adapter;

/**
 * Created by usuario on 28/07/16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
