package br.com.gpm.concretegithub.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import br.com.gpm.concretegithub.R;
import br.com.gpm.concretegithub.rest.model.PullRequestItem;

/**
 * Created by usuario on 01/08/16.
 */
public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestViewHolder> {
    private List<PullRequestItem> list = Collections.emptyList();
    private Context context;

    public PullRequestAdapter(List<PullRequestItem> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        PullRequestViewHolder viewHolder;

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout_pulls, viewGroup, false);
        viewHolder = new PullRequestViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, int i) {
        holder.pullTitle.setText(list.get(i).getTitle());
        holder.pullBody.setText(list.get(i).getBody());
        holder.pullUserLogin.setText(list.get(i).getUser().getLogin());
        Picasso.with(context)
                .load(list.get(i).getUser().getAvatarUrl())
                .placeholder(R.drawable.github_placeholder)
                .into((holder.pullUserAvatar));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

