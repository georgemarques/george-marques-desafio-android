package br.com.gpm.concretegithub.rest.model;

import java.util.List;

/**
 * Created by usuario on 25/07/16.
 */
public class PullResponse {
    List<PullRequestItem> pulls;

    public List<PullRequestItem> getPulls() {
        return pulls;
    }

    public void setPulls(List<PullRequestItem> pulls) {
        this.pulls = pulls;
    }
}
